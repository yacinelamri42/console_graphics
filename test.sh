#!/usr/bin/env sh

if [ -z $PROJECT_ROOT ] && [ -d ".git" ]; then
    export PROJECT_ROOT=$(pwd)
elif [ -z $PROJECT_ROOT ] ; then
    echo "aa"
    exit 1
fi

$PROJECT_ROOT/build.sh &&
    $PROJECT_ROOT/tests/runtests.sh
