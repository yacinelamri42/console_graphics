#!/usr/bin/env sh

if [ -z $PROJECT_ROOT ]; then
    echo "PROJECT_ROOT not found"
    exit
fi
[ -d "$PROJECT_ROOT/console_graphics_ansi/bin" ] || mkdir "$PROJECT_ROOT/console_graphics_ansi/bin"

for item in "$PROJECT_ROOT/console_graphics_ansi/src"/*.c; do
    $CC $CFLAGS -c "$item" -o "$PROJECT_ROOT/console_graphics_ansi/bin/$(basename "$item").o"
done &&
    ar rcs "$PROJECT_ROOT/console_graphics_ansi/bin/libconsole_graphics_ansi.a" "$PROJECT_ROOT/console_graphics_ansi/bin"/*.o &&
    cp "$PROJECT_ROOT/console_graphics_ansi/bin/libconsole_graphics_ansi.a" "$PROJECT_ROOT/libs/libconsole_graphics_ansi.a"
