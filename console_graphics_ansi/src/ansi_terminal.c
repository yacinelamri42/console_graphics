// FIND A BETTER WAY TO SOLVE THIS PROBLEM!!!
#include "../../include/console_graphics_ansi.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>

void ansi_set_title(const char* title) {
	printf("\e]0;%s%c", title, 0x07);
}

void ansi_set_color_8(AnsiColor8 color, AnsiColorPosition color_position) {
	char c = (color_position == FOREGROUND) ? '3' : '4';
	printf("\e[%c%dm", c, color);
}

void ansi_set_color_16(AnsiColor16 color, AnsiColorPosition color_position) {
	char c = (color_position == FOREGROUND) ? '3' : '4';
	char str[3] = {0};
	if (color > 7) {
		str[0] = ';';
		str[1] = '1';
		color-=8;
	}
	printf("\e[%c%d%sm", c, color, str);
}

void ansi_set_color_256(AnsiColor256 color, AnsiColorPosition color_position) {
	char c = (color_position == FOREGROUND) ? '3' : '4';
	printf("\e[%c8;5;%dm", c, color);
}

void ansi_set_color_rgb(unsigned char red, unsigned char green, unsigned char blue, AnsiColorPosition color_position) {
	char c = (color_position == FOREGROUND) ? '3' : '4';
	printf("\e[%c8;2;%d;%d;%dm", c, red, green, blue);
}

void ansi_set_attribute(AnsiAttribute attribute) {
    printf("\e[%dm", attribute);
}

void ansi_move_to(unsigned int row, unsigned int column) {
	printf("\e[%d;%dH", row, column);
}

void ansi_move(AnsiMovementDirection direction, int distance) {
	if (distance < 0) {
		distance = -distance;
		if (direction == UP || direction == RIGHT) {
			direction++;
		}else{
			direction--;
		}
	}
	printf("\e[%d%c", distance, direction);
}

void ansi_clear_screen() {
	printf("\e[2J");
}

void ansi_open_alternative_screen() {
	printf("\e[?1049h");
	ansi_move_to(1, 1);
}

void ansi_close_alternative_screen() {
	printf("\e[?1049l");
	ansi_reset();
}

void ansi_reset() {
	printf("\e[0m");
}

void ansi_show_cursor() {
	printf("\e[?25h");
}

void ansi_hide_cursor() {
	printf("\e[?25l");
}

void ansi_get_console_size(size_t* number_of_rows, size_t* number_of_columns) {
	struct winsize size;
	ioctl(0, TIOCGWINSZ, &size);
	*number_of_rows = size.ws_row;
	*number_of_columns = size.ws_col;
}

void ansi_set_console_size(size_t number_of_rows, size_t number_of_columns) {
	struct winsize size = {.ws_col=number_of_columns, .ws_row = number_of_rows};
	ioctl(0, TIOCSWINSZ, size);
}

AnsiSurfaceError ansi_create_surface(AnsiSurface* surface, void* buffer, int width, int height, AnsiBufferType buffer_type) {
	assert(buffer && "buffer is null");
	assert(width  && "width is 0");
	assert(height && "height is 0");
	/* assert(buffer_type && "the buffer does not exist"); */
	switch (buffer_type) {
		case BUFFER_TYPE_FOREGROUND3_CHAR_BACKGROUND3:
			surface->stride = width*sizeof(BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3);
			surface->pixel_size = sizeof(BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3);
			break;
		case BUFFER_TYPE_NO_BUFFER:
			return SURFACE_ERROR_NO_BUFFER;
			break;
	}
	surface->buffer_type = buffer_type;
	surface->buffer = buffer;
	surface->width = width;
	surface->height = height;
	return SURFACE_ERROR_NO_ERROR;
}

AnsiSurfaceError ansi_print_surface_FOREGROUND3_CHAR_BACKGROUND3(AnsiSurface* surface) {
	BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3* buffer = (BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3*)surface->buffer;
	unsigned int current_row = 1;
	unsigned int current_column = 1;
	for (int i=0; i<surface->height; i++) {
		for (int j=0; j<surface->width; j++) {
                  ansi_move_to(current_row, current_column);
                  ansi_set_color_rgb(buffer->foreground_color.red,
                                     buffer->foreground_color.green,
                                     buffer->foreground_color.blue, FOREGROUND);
                  ansi_set_color_rgb(buffer->background_color.red,
                                     buffer->background_color.green,
                                     buffer->background_color.blue, BACKGROUND);
                  printf("%c", buffer->c);
                  ansi_reset();
                  current_column++;
                  buffer++;
		}
		current_column = 1;
		current_row++;
	}
	return SURFACE_ERROR_NO_ERROR;
}


AnsiSurfaceError ansi_print_surface(AnsiSurface *surface) {
	switch (surface->buffer_type) {
		case BUFFER_TYPE_FOREGROUND3_CHAR_BACKGROUND3:
			return ansi_print_surface_FOREGROUND3_CHAR_BACKGROUND3(surface);
			break;
		case BUFFER_TYPE_NO_BUFFER:
			return SURFACE_ERROR_NO_BUFFER;
			break;
	}
	return SURFACE_ERROR_NO_ERROR;
}

void ansi_destroy_surface(AnsiSurface* surface) {
	free(surface->buffer);
	surface->buffer = NULL;
	surface->height = 0;
	surface->width = 0;
}
