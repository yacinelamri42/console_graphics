#!/usr/bin/env sh

if [ -z $PROJECT_ROOT ] && [ -d ".git" ]; then
    export PROJECT_ROOT=$(pwd)
elif [ -z $PROJECT_ROOT ]; then
    echo bb
    exit 1
fi
export CC="gcc"
export CFLAGS="-Wall -g -fsanitize=address -std=c11 -I./include/"

"$PROJECT_ROOT/console_graphics_ansi/build.sh"
"$PROJECT_ROOT/console_graphics_render/build.sh"
"$PROJECT_ROOT/console_graphics_keyboard/build.sh"
