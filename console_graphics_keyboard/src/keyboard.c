#include "../../include/console_graphics_keyboard.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>

static struct termios original_config;

static void die(const char* final_error_message) {
    perror(final_error_message);
    exit(1);
}

void console_graphics_keyboard_set_raw() {
    struct termios config;
    if(tcgetattr(STDIN_FILENO, &config)) {
        die("tcgetattr");
    }
    config.c_iflag &= ~(BRKINT | INPCK | ISTRIP | IXON | ICRNL);
    config.c_oflag &= ~(OPOST);
    config.c_cflag |= (CS8);
    config.c_lflag &= ~(ECHO | ICANON | ISIG | IEXTEN);
    config.c_cc[VMIN] = 0;
    config.c_cc[VTIME] = 1;
    if (tcsetattr(STDIN_FILENO, TCSADRAIN, &config)) {
        die("tcsetattr not working");
    }
}

void console_graphics_keyboard_unset_raw() {
    if (tcsetattr(STDIN_FILENO, TCSADRAIN, &original_config)) {
        die("tcsetattr not working");
    }
}

uint16_t console_graphics_keyboard_get_key() {
    char c;
    int err;
    while ((err=read(STDIN_FILENO, &c, 1)) != 1) {
        if (err == -1) {
            die("couldnt read");
        }
    }
    if (c == 0x1b) {
        char sequence[3];
        if (read(STDIN_FILENO, &sequence[0], 1) != 1) {
            return 0x1b;
        }
        if (read(STDIN_FILENO, &sequence[1], 1) != 1) {
            return 0x1b;
        }
        if (sequence[0] == '[') {
            switch (sequence[1]) {
                case 'A': return KEY_UP_ARROW;
                case 'B': return KEY_DOWN_ARROW;
                case 'C': return KEY_RIGHT_ARROW;
                case 'D': return KEY_LEFT_ARROW;
            }
        }
    }
    return c;
}
