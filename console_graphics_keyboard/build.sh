#!/usr/bin/env sh

if [ -z $PROJECT_ROOT ]; then
    echo "PROJECT_ROOT not found"
    exit
fi
[ -d "$PROJECT_ROOT/console_graphics_keyboard/bin" ] || mkdir "$PROJECT_ROOT/console_graphics_keyboard/bin"

for item in "$PROJECT_ROOT/console_graphics_keyboard/src"/*.c; do
    $CC $CFLAGS -c "$item" -o "$PROJECT_ROOT/console_graphics_keyboard/bin/$(basename "$item").o"
done &&
    ar rcs "$PROJECT_ROOT/console_graphics_keyboard/bin/libconsole_graphics_keyboard.a" "$PROJECT_ROOT/console_graphics_keyboard/bin"/*.o &&
    cp "$PROJECT_ROOT/console_graphics_keyboard/bin/libconsole_graphics_keyboard.a" "$PROJECT_ROOT/libs/libconsole_graphics_keyboard.a"
