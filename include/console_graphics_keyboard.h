#ifndef CONSOLE_GRAPHICS_KEYBOARD_H_
#define CONSOLE_GRAPHICS_KEYBOARD_H_

#include <stdint.h>
#define KEY_CTRL(key) (key & 0x1f)

#define KEY_UP_ARROW      256
#define KEY_DOWN_ARROW    257
#define KEY_LEFT_ARROW    258
#define KEY_RIGHT_ARROW   259
#define KEY_DELETE        260
#define KEY_PAGE_UP       261
#define KEY_PAGE_DOWN     262
#define KEY_HOME          263
#define KEY_END           263

void console_graphics_keyboard_set_raw();
void console_graphics_keyboard_unset_raw();
uint16_t console_graphics_keyboard_get_key();

#endif // CONSOLE_GRAPHICS_KEYBOARD_H_
