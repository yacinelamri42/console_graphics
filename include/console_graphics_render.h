#ifndef CONSOLE_GRAPHICS_RENDER_H_
#define CONSOLE_GRAPHICS_RENDER_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "console_graphics_ansi.h"

/* CGR stands for Console Graphics Render */

typedef struct {
    size_t x;
    size_t y;
} CGR_Point;

typedef struct {
    size_t width;
    size_t height;
    size_t pixel_size;
    void* front_buffer;
    void* back_buffer;
} CGR_Surface;

typedef struct {
    size_t width;
    size_t height;
    CGR_Surface surface;
} CGR_Window ;

typedef enum {
    CGR_WINDOW_ERROR_NO_ERROR          = 0,
    CGR_WINDOW_ERROR_WIDTH_IS_0        = 1,
    CGR_WINDOW_ERROR_HEIGHT_IS_0       = 2,
} CGR_WindowError;

typedef enum {
    CGR_SURFACE_ERROR_NO_ERROR            = 0,
    CGR_SURFACE_ERROR_ALLOCATION_ERROR    = 1,
    CGR_SURFACE_ERROR_OUT_OF_BOUNDS_ERROR = 2,
} CGR_SurfaceError;

typedef enum {
    CGR_DRAWING_ERROR_NO_ERROR         = 0,
    CGR_DRAWING_ERROR_NO_SURFACE       = 1,
    CGR_DRAWING_ERROR_OUT_OF_BOUNDS    = 2,
} CGR_DrawingError;

typedef enum {
    CGR_FILE_ERROR_NO_ERROR            = 0,
    CGR_FILE_COULD_NOT_OPEN_FILE       = 1, // check errno in this case for more detail!
    CGR_FILE_WRONG_HEADER              = 2,
    CGR_FILE_WRONG_FILE_SIZE           = 3,
} CGR_FileError;

typedef enum {
    CGR_BITMAP_TYPE_FOREGROUND3_CHAR_BACKGROUND3,
} CGR_BitMapType;

typedef struct {
    char first_8_bytes[8]; // Should always be ['C', 'G', 'R', 'B', 'I', 'T', 'M', 'A'] or else file is not recongnized
    CGR_BitMapType bitmap_type;
    size_t width;
    size_t height;
} CGR_BitMapHeader;

/* typedef enum { */
/*     CGR_SURFACE_RESIZING_DIRECTION_UP      = 1 << 0, */
/*     CGR_SURFACE_RESIZING_DIRECTION_DOWN    = 1 << 1, */
/*     CGR_SURFACE_RESIZING_DIRECTION_LEFT    = 1 << 2, */
/*     CGR_SURFACE_RESIZING_DIRECTION_RIGHT   = 1 << 3, */
/* } CGR_SurfaceResizingDirection; */

typedef enum {
    CGR_SURFACE_RESIZING_ERROR_NO_ERROR,
} CGR_SurfaceResizingError;

bool cgr_is_on();
// if width and height are 0, check terminal size
CGR_WindowError cgr_open_window(CGR_Window* window, const char* title, size_t width, size_t height);
void cgr_close_window(CGR_Window* window);

CGR_SurfaceError cgr_create_surface(CGR_Surface* surface, CGR_Window* window, size_t width, size_t height, size_t pixel_size);
void cgr_destroy_surface(CGR_Surface* surface);

CGR_DrawingError cgr_draw_point(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point point);
CGR_DrawingError cgr_draw_line(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point point1, CGR_Point point2);

// the 2 points are 2 diagonally opposite corners from the rectangle
CGR_DrawingError cgr_draw_rectangle(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point point1, CGR_Point point2);

CGR_DrawingError cgr_draw_triangle(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point point1, CGR_Point point2, CGR_Point point3);

CGR_DrawingError cgr_draw_circle(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point point1, size_t radius);

CGR_DrawingError cgr_draw_surface_at(CGR_Surface* destination, CGR_Surface* source, CGR_Point top_corner);

void cgr_begin_drawing(CGR_Window* window);
void cgr_end_drawing(CGR_Window* window);

CGR_FileError cgr_load_surface(CGR_Window* window, CGR_Surface* surface, const char* filename);
CGR_FileError cgr_save_surface(CGR_Surface* surface, const char* filename, CGR_BitMapType bitmap_type);

CGR_SurfaceResizingError cgr_resize_surface(CGR_Surface* surface, size_t new_width, size_t new_height);

#endif // CONSOLE_GRAPHICS_RENDER_H_
