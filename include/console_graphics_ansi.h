#ifndef CONSOLE_GRAPHICS_ANSI_H_
#define CONSOLE_GRAPHICS_ANSI_H_

#include <stdint.h>
#include <stddef.h>

typedef enum {
    FOREGROUND,
    BACKGROUND,
} AnsiColorPosition;

typedef enum {
    BUFFER_TYPE_NO_BUFFER                     = 0,
    BUFFER_TYPE_FOREGROUND3_CHAR_BACKGROUND3 = 1
} AnsiBufferType;

typedef enum {
    BLACK_8              = 0,
    RED_8                = 1,
    GREEN_8              = 2,
    YELLOW_8             = 3,
    BLUE_8               = 4,
    MAGENTA_8            = 5,
    CYAN_8               = 6,
    WHITE_8              = 7,
} AnsiColor8;


typedef enum {
    BLACK_16             = 0,
    RED_16               = 1,
    GREEN_16             = 2,
    YELLOW_16            = 3,
    BLUE_16              = 4,
    MAGENTA_16           = 5,
    CYAN_16              = 6,
    WHITE_16             = 7,
    BRIGHT_BLACK_16      = 8,
    BRIGHT_RED_16        = 9,
    BRIGHT_GREEN_16      = 10,
    BRIGHT_YELLOW_16     = 11,
    BRIGHT_BLUE_16       = 12,
    BRIGHT_MAGENTA_16    = 13,
    BRIGHT_CYAN_16       = 14,
    BRIGHT_WHITE_16      = 15,
} AnsiColor16;

typedef unsigned char AnsiColor256;

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} AnsiColorRGB;

typedef enum {
    BOLD                 = 1,
    ITALIC               = 3,
    UNDERLIGNED          = 4,
    INVERSECOLOR         = 7,
} AnsiAttribute;

typedef enum {
    UP                   = 'A',
    DOWN                 = 'B',
    RIGHT                = 'C',
    LEFT                 = 'D',
} AnsiMovementDirection;

typedef struct {
    void* buffer;
    unsigned int width;
    unsigned int height;
    unsigned int stride;
    size_t pixel_size;
    AnsiBufferType buffer_type;
} AnsiSurface;

typedef enum {
    SURFACE_ERROR_NO_ERROR  = 0,
    SURFACE_ERROR_NO_BUFFER = 1,
} AnsiSurfaceError;

typedef struct {
    AnsiColorRGB foreground_color;
    char c;
    AnsiColorRGB background_color;
} BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3;

void ansi_set_color_8(AnsiColor8 color, AnsiColorPosition color_position);
void ansi_set_color_16(AnsiColor16 color, AnsiColorPosition color_position);
void ansi_set_color_256(AnsiColor256 color, AnsiColorPosition color_position);
void ansi_set_color_rgb(unsigned char red, unsigned char green, unsigned char blue, AnsiColorPosition color_position);
void ansi_set_attribute(AnsiAttribute attribute);
void ansi_move(AnsiMovementDirection direction, int distance);
void ansi_move_to(unsigned int row, unsigned int column);
void ansi_open_alternative_screen();
void ansi_close_alternative_screen();
void ansi_show_cursor();
void ansi_hide_cursor();
void ansi_clear_screen();
void ansi_reset();

void ansi_set_title(const char* title);
void ansi_get_console_size(size_t* number_of_rows, size_t* number_of_columns);
void ansi_set_console_size(size_t number_of_rows, size_t number_of_columns);

AnsiSurfaceError ansi_create_surface(AnsiSurface* surface, void* buffer, int width, int height, AnsiBufferType buffer_type);
AnsiSurfaceError ansi_print_surface(AnsiSurface* surface);
void ansi_destroy_surface(AnsiSurface* surface);


#define ANSI_PRINT_WITH_COLOR_8(foreground_color, background_color, string, ...) \
	ansi_set_color_8(foreground_color, FOREGROUND);     \
	ansi_set_color_8(background_color, BACKGROUND);     \
	printf(string, ##__VA_ARGS__);						\
	ansi_reset();										\

#define ANSI_PRINT_WITH_COLOR_16(foreground_color, background_color, string, ...) \
	ansi_set_color_16(foreground_color, FOREGROUND);     \
	ansi_set_color_16(background_color, BACKGROUND);     \
	printf(string, ##__VA_ARGS__);						\
	ansi_reset();										\

#define ANSI_PRINT_WITH_COLOR_256(foreground_color, background_color, string, ...) \
	ansi_set_color_256(foreground_color, FOREGROUND);     \
	ansi_set_color_256(background_color, BACKGROUND);     \
	printf(string, ##__VA_ARGS__);						\
	ansi_reset();										\

#define ANSI_PRINT_WITH_COLOR_RGB(foreground_red, foreground_green, foreground_blue, background_red, background_green, background_blue, string, ...) \
	ansi_set_color_rgb(foreground_red, foreground_green, foreground_blue, FOREGROUND);     \
	ansi_set_color_rgb(background_red, background_green, foreground_blue, BACKGROUND);     \
	printf(string, ##__VA_ARGS__);						\
	ansi_reset();										\

#endif // CONSOLE_GRAPHICS_ANSI_H_
