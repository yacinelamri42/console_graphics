#include "../../include/console_graphics_render.h"
#include <assert.h>
#include <signal.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

static bool cgr_on = true;

bool cgr_is_on() {
    return cgr_on;
}

void cgr_sigint_handler(int a) {
    cgr_on=false;
}

// if width and height are 0, check terminal size
CGR_WindowError cgr_open_window(CGR_Window* window, const char* title, size_t width, size_t height) {
    signal(SIGINT, cgr_sigint_handler);
    if (width == 0 && height == 0) {
        ansi_get_console_size(&height, &width);
    }
    if (width == 0) {
        return CGR_WINDOW_ERROR_WIDTH_IS_0;
    }
    if (height == 0) {
        return CGR_WINDOW_ERROR_HEIGHT_IS_0;
    }
    if (title) {
        ansi_set_title(title);
    }
    window->height = height;
    window->width = width;
    ansi_hide_cursor();
    CGR_Surface surface;
    // i set the pixel size as 8 temporarily
    if(cgr_create_surface(&surface, window, window->width, window->height, sizeof(BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3))) {
        fprintf(stderr, "cgr_create_surface failed");
        exit(1);
    }
    window->surface = surface;
    ansi_open_alternative_screen();
    ansi_clear_screen();
    return CGR_WINDOW_ERROR_NO_ERROR;
}

void cgr_close_window(CGR_Window* window) {
    cgr_destroy_surface(&(window->surface));
    window->height = 0;
    window->width = 0;
    ansi_show_cursor();
    ansi_close_alternative_screen();
}

CGR_SurfaceError cgr_create_surface(CGR_Surface* surface, CGR_Window* window, size_t width, size_t height, size_t pixel_size) {
    if (height > window->height || width > window->width) {
        return CGR_SURFACE_ERROR_OUT_OF_BOUNDS_ERROR;
    }
    assert(pixel_size);
    surface->pixel_size = pixel_size;
    surface->height = height;
    surface->width = width;
    size_t size_of_buffer = width*height;
    assert(size_of_buffer > 0);
    void* back_buffer = calloc(size_of_buffer, pixel_size);
    void* front_buffer = calloc(size_of_buffer, pixel_size);
    if (!front_buffer || !back_buffer) {
        return CGR_SURFACE_ERROR_ALLOCATION_ERROR;
    }
    surface->back_buffer = back_buffer;
    surface->front_buffer = front_buffer;
    return CGR_SURFACE_ERROR_NO_ERROR;
}

void cgr_destroy_surface(CGR_Surface* surface) {
    free(surface->back_buffer);
    free(surface->front_buffer);
    surface->height = 0;
    surface->width = 0;
    surface->pixel_size = 0;
}

CGR_DrawingError cgr_draw_point(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point point) {
    if (point.x > surface->width || point.y > surface->height) {
        return CGR_DRAWING_ERROR_OUT_OF_BOUNDS;
    }
    BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3* buffer = surface->back_buffer;
    buffer+=((surface->width*point.y)+point.x);
    buffer->background_color = background;
    buffer->foreground_color = foreground;
    buffer->c = c;
    return CGR_DRAWING_ERROR_NO_ERROR;
}

CGR_DrawingError cgr_draw_line(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point point0, CGR_Point point1) {
    if (point0.x > surface->width || point1.x > surface->width || point0.y > surface->height || point1.y > surface->height) {
        return CGR_DRAWING_ERROR_OUT_OF_BOUNDS;
    }
    int32_t dx = abs((int)point1.x - (int)point0.x);
    int32_t dy = -abs((int)point1.y - (int)point0.y);
    int32_t sx = (point0.x < point1.x) ? 1 : -1;
    int32_t sy = (point0.y < point1.y) ? 1 : -1;
    int32_t err = dx+dy;
    int32_t e2;
    while (1) {
        cgr_draw_point(surface, c, foreground, background, point0);
        if (point0.x == point1.x && point0.y == point1.y) {
            break;
        }
        e2 = 2*err;
        if (e2 >= dy) {
            err+=dy;
            point0.x+=sx;
        }
        if (e2 <= dx) {
            err+=dx;
            point0.y+=sy;
        }
    }
    return CGR_DRAWING_ERROR_NO_ERROR;
}

// the 2 points are 2 diagonally opposite corners from the rectangle
CGR_DrawingError cgr_draw_rectangle(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point point0, CGR_Point point1) {
    CGR_Point p0 = point0;
    CGR_Point p1 = {.x=point1.x, .y=point0.y};
    CGR_Point p2 = {.x=point0.x, .y=point1.y};
    CGR_Point p3 = point1;
    if ((cgr_draw_line(surface, c, foreground, background, p0,  p1)
          || cgr_draw_line(surface, c, foreground, background, p0,  p2)
          || cgr_draw_line(surface, c, foreground, background, p1,  p3)
          || cgr_draw_line(surface, c, foreground, background, p2,  p3)) ){
        return CGR_DRAWING_ERROR_OUT_OF_BOUNDS;
    }
    return CGR_DRAWING_ERROR_NO_ERROR;
}

CGR_DrawingError cgr_draw_triangle(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point point0, CGR_Point point1, CGR_Point point2) {

    if ((cgr_draw_line(surface, c, foreground, background, point0,  point1)
          || cgr_draw_line(surface, c, foreground, background, point1,  point2)
          || cgr_draw_line(surface, c, foreground, background, point0,  point2)) ){
        return CGR_DRAWING_ERROR_OUT_OF_BOUNDS;
    }
    return CGR_DRAWING_ERROR_NO_ERROR;
}

CGR_DrawingError draw_4_points(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point center, int dx, int dy) {
    CGR_DrawingError err0 = cgr_draw_point(surface, c, foreground, background, (CGR_Point){.x=center.x+dx, .y=center.y+dy});
    CGR_DrawingError err1 = cgr_draw_point(surface, c, foreground, background, (CGR_Point){.x=center.x-dx, .y=center.y+dy});
    CGR_DrawingError err2 = cgr_draw_point(surface, c, foreground, background, (CGR_Point){.x=center.x+dx, .y=center.y-dy});
    CGR_DrawingError err3 = cgr_draw_point(surface, c, foreground, background, (CGR_Point){.x=center.x-dx, .y=center.y-dy});
    CGR_DrawingError err4 = cgr_draw_point(surface, c, foreground, background, (CGR_Point){.x=center.x+dy, .y=center.y+dx});
    CGR_DrawingError err5 = cgr_draw_point(surface, c, foreground, background, (CGR_Point){.x=center.x-dy, .y=center.y+dx});
    CGR_DrawingError err6 = cgr_draw_point(surface, c, foreground, background, (CGR_Point){.x=center.x+dy, .y=center.y-dx});
    CGR_DrawingError err7 = cgr_draw_point(surface, c, foreground, background, (CGR_Point){.x=center.x-dy, .y=center.y-dx});
    if ((err0+err1+err2+err3+err4+err5+err6+err7)) {
        return CGR_DRAWING_ERROR_OUT_OF_BOUNDS;
    }
    return CGR_DRAWING_ERROR_NO_ERROR;
}

CGR_DrawingError cgr_draw_circle(CGR_Surface* surface, char c, AnsiColorRGB foreground, AnsiColorRGB background, CGR_Point center, size_t radius) {
    int x = 0;
    int y = radius;
    int d = 3-2*radius;
    CGR_DrawingError err = draw_4_points(surface, c, foreground, background, center, x, y);
    while (y>=x) {
        if (err) {
            return err;
        }
        x++;
        if (d > 0) {
            y--;
            d = d + 4 * (x - y) + 10;
        }else{
            d = d + 4 * x + 6;
        }
        err = draw_4_points(surface, c, foreground, background, center, x, y);
    }
    return CGR_DRAWING_ERROR_NO_ERROR;
}

CGR_SurfaceResizingError cgr_resize_surface(CGR_Surface* surface, size_t new_width, size_t new_height) {

    return CGR_SURFACE_RESIZING_ERROR_NO_ERROR;
}

CGR_DrawingError cgr_draw_surface_at(CGR_Surface* destination, CGR_Surface* source, CGR_Point pos_of_top_corner_of_source_in_destination) {
    if (destination->height < source->height || destination->width < source->width) {
        return CGR_DRAWING_ERROR_OUT_OF_BOUNDS;
    }
    BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3* src = source->back_buffer;
    BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3* dest = destination->back_buffer;
    dest+=pos_of_top_corner_of_source_in_destination.y*destination->width+pos_of_top_corner_of_source_in_destination.x;
    for (int row=0; row<source->height; row++) {
        for (int col=0; col<source->width; col++) {
            *dest++ = *src++;
        }
        dest+=destination->width-source->width;
    }
    return CGR_DRAWING_ERROR_NO_ERROR;
}

void cgr_begin_drawing(CGR_Window* window) {
    memset(window->surface.back_buffer,0,window->height*window->width*window->surface.pixel_size);
}

void cgr_end_drawing(CGR_Window* window) {
    if (!memcmp(window->surface.back_buffer, window->surface.front_buffer, window->height*window->width*window->surface.pixel_size)) {
        return;
    }
    ansi_clear_screen();
    memmove(window->surface.front_buffer, window->surface.back_buffer, window->surface.height*window->surface.width*window->surface.pixel_size);
    AnsiSurface surface;
    AnsiSurfaceError ansi_surface_error = ansi_create_surface(&surface, window->surface.front_buffer, window->width, window->height, BUFFER_TYPE_FOREGROUND3_CHAR_BACKGROUND3);
    ansi_print_surface(&surface);

}
