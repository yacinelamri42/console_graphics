// FIND A WAY TO MAKE THIS BETTER!!!!
#include "/home/yacine/programming/C/console_graphics/include/console_graphics_render.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>


/* typedef struct { */
/*     char first_8_bytes[8]; // Should always be ['C', 'G', 'R', 'B', 'I', 'T', 'M', 'A'] or else file is not recongnized */
/*     CGR_BitMapType bitmap_type; */
/*     uint16_t width; */
/*     uint16_t height; */
/* } CGR_BitMapHeader; */

CGR_FileError cgr_load_surface(CGR_Window* window, CGR_Surface* surface, const char* filename) {
    FILE* file = fopen(filename, "r");
    if (!file) {
        return CGR_FILE_COULD_NOT_OPEN_FILE;
    }
    CGR_BitMapHeader file_header;
    if(!fread(&file_header, sizeof(CGR_BitMapHeader), 1, file)) {
        return CGR_FILE_WRONG_HEADER;
    }
    if (strncmp(file_header.first_8_bytes, "CGRBITMA", 8)) {
        return CGR_FILE_WRONG_HEADER;
    }

    size_t number_of_pixels = file_header.height*file_header.width;
    size_t file_size = sizeof(file_header);
    size_t pixel_size = 0;
    switch (file_header.bitmap_type) {
        case CGR_BITMAP_TYPE_FOREGROUND3_CHAR_BACKGROUND3:
            pixel_size = sizeof(BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3);
            break;
    }
    file_size+=(number_of_pixels*pixel_size);
    size_t real_file_size = 0;
    fseek(file, 0, SEEK_SET);
    for (; fgetc(file)!=EOF; real_file_size++);
    if (file_size != real_file_size) {
        return CGR_FILE_WRONG_FILE_SIZE;
    }
    surface->height = file_header.height;
    surface->width = file_header.width;
    surface->pixel_size = pixel_size;
    memset(surface->front_buffer, 0, surface->pixel_size*surface->height*surface->width);
    fseek(file, sizeof(CGR_BitMapHeader), SEEK_SET);
    fread(surface->back_buffer, pixel_size, number_of_pixels, file);
    fclose(file);
    return CGR_FILE_ERROR_NO_ERROR;
}

CGR_FileError cgr_save_surface(CGR_Surface* surface, const char* filename, CGR_BitMapType bitmap_type) {
    FILE* file = fopen(filename, "w");
    if (!file) {
        return CGR_FILE_COULD_NOT_OPEN_FILE;
    }
    CGR_BitMapHeader file_header;
    memcpy(file_header.first_8_bytes, "CGRBITMA", 8);
    file_header.bitmap_type = bitmap_type;
    file_header.height = surface->height;
    file_header.width = surface->width;
    fwrite(&file_header, sizeof(file_header), 1, file);
    fwrite(surface->back_buffer, surface->pixel_size, surface->width*surface->height, file);
    fclose(file);
    return CGR_FILE_ERROR_NO_ERROR;
}
