#!/usr/bin/env sh

if [ -z $PROJECT_ROOT ]; then
    echo "PROJECT_ROOT not found"
    exit
fi
[ -d "$PROJECT_ROOT/console_graphics_render/bin" ] || mkdir "$PROJECT_ROOT/console_graphics_render/bin"

for item in "$PROJECT_ROOT/console_graphics_render/src"/*.c; do
    $CC $CFLAGS -c "$item" -o "$PROJECT_ROOT/console_graphics_render/bin/$(basename "$item").o"
done &&
    ar rcs "$PROJECT_ROOT/console_graphics_render/bin/libconsole_graphics_render.a" "$PROJECT_ROOT/console_graphics_render/bin"/*.o &&
    cp "$PROJECT_ROOT/console_graphics_render/bin/libconsole_graphics_render.a" "$PROJECT_ROOT/libs/libconsole_graphics_render.a"
