#include "../../../include/console_graphics_keyboard.h"
#include "../../../include/console_graphics_render.h"
#include <stdlib.h>

int main(int argc, char *argv[]) {
    CGR_Window window;
    if(cgr_open_window(&window, "keyboard test", 0, 0)) {
        return 1;
    }
    CGR_Surface surface;
    AnsiColorRGB fg = {.blue=128,.green=127};
    AnsiColorRGB bg = {.red=128,.green=127};
    if (cgr_create_surface(&surface, &window, 1, 1, sizeof(BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3))) {
        return 1;
    }
    cgr_draw_point(&surface, 'C', fg, bg, (CGR_Point){.x=0,.y=0});
    size_t x = window.width/2;
    size_t y = window.height/2;
    console_graphics_keyboard_set_raw();
    atexit(console_graphics_keyboard_unset_raw);
    while (cgr_is_on()) {
        cgr_begin_drawing(&window);
        cgr_draw_surface_at(&(window.surface), &surface, (CGR_Point){.x=x,.y=y});
        cgr_end_drawing(&window);
        uint16_t key = console_graphics_keyboard_get_key();
        if (key == KEY_CTRL('q')) {
            break;
        }
        switch (key) {
            case KEY_UP_ARROW:
                y--;
                break;
            case KEY_DOWN_ARROW:
                y++;
                break;
            case KEY_LEFT_ARROW:
                x--;
                break;
            case KEY_RIGHT_ARROW:
                x++;
                break;
        }
    }
    console_graphics_keyboard_unset_raw();
    cgr_close_window(&window);
    return 0;
}
