#!/usr/bin/env sh


if [ -z $PROJECT_ROOT ]; then
    echo "PROJECT_ROOT not found"
    exit
fi

"$PROJECT_ROOT/tests/console_graphics_keyboard_tests/build-tests.sh" &&
    "$PROJECT_ROOT/tests/console_graphics_keyboard_tests/bin/test"
