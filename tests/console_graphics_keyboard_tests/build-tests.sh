#!/usr/bin/env sh


if [ -z $PROJECT_ROOT ]; then
    echo "PROJECT_ROOT not found"
    exit
fi
CC="gcc"
LD="gcc"
CFLAGS="-Wall -g"
LDFLAGS="-fsanitize=address -L$PROJECT_ROOT/libs/"
LIBS=" -lconsole_graphics_render -lconsole_graphics_ansi -lconsole_graphics_keyboard -lc"


for item in "$PROJECT_ROOT/tests/console_graphics_keyboard_tests/src"/*.c; do
	$CC $CFLAGS -c $item -o $item.o
done && $LD $LDFLAGS "$PROJECT_ROOT/tests/console_graphics_keyboard_tests/src"/*.o -o "$PROJECT_ROOT/tests/console_graphics_keyboard_tests/bin/test" $LIBS
