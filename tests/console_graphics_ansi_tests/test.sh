#!/usr/bin/env sh


if [ -z $PROJECT_ROOT ]; then
    echo "PROJECT_ROOT not found"
    exit
fi

"$PROJECT_ROOT/tests/console_graphics_ansi_tests/build-tests.sh" &&
    "$PROJECT_ROOT/tests/console_graphics_ansi_tests/bin/test"
