#include "../../../include/console_graphics_ansi.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {
    uint16_t num_rows;
    uint16_t num_cols;
    ansi_open_alternative_screen();
    ansi_set_title("my title");
    ansi_get_console_size(&num_rows, &num_cols);
    void* buffer = calloc(num_rows*num_cols, 8);
    AnsiSurface surface;
    ansi_create_surface(&surface, buffer, num_cols, num_rows, BUFFER_TYPE_FOREGROUND3_CHAR_BACKGROUND3);
    while (1) {
        BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3* buf = buffer;
        buf+=10*num_cols+5;
        buf->background_color.blue = 128;
        buf->foreground_color.red = 128;
        buf->c = 'A';
        ansi_print_surface(&surface);
        sleep(1);
    }
    ansi_destroy_surface(&surface);
    getchar();
    ansi_close_alternative_screen();
    return 0;
}
