#include "../../../include/console_graphics_render.h"
#include <stdio.h>
#include <stdbool.h>


int main(void) {
    CGR_Window window;
    cgr_open_window(&window, "console graphics render", 0, 0);
    CGR_Surface surface;
    if (cgr_create_surface(&surface, &window, 20, 20, sizeof(BufferEncoding_FOREGROUND3_CHAR_BACKGROUND3))) {
        return 1;
    }
    printf("%ld %ld\n", window.width, window.height);
    getchar();
    int i=0;
    int j = 0;
    AnsiColorRGB foreground = {.red = 128, .blue = 0, .green = 0};
    AnsiColorRGB background = {.red = 0, .blue = 128, .green = 0};
    CGR_Point point1 = {.x = 5, .y = 2};
    CGR_Point point2 = {.x = 100, .y = 40};
    CGR_Point point3 = {.x = 12, .y = 39};
    cgr_load_surface(&window, &surface, "surface");
    while (cgr_is_on()) {
        cgr_begin_drawing(&window);
        cgr_end_drawing(&window);
    }
    cgr_destroy_surface(&surface);
    cgr_close_window(&window);
    return 0;
}
